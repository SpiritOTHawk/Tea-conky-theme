# Tea-conky-theme
Created : 2024/Jan/31
This theme is for conky version 1.10.8 or newer
Tea ( Fork of Mimosa theme)
author : SpiritOTHawk
original-author  : Closebox73
license : GPL-v3.0-or-later
tested on resolution: FHD 16:9
# Screenshot
![prev](https://i.ibb.co/y48Y7G3/image.png)